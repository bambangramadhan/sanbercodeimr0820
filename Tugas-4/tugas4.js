// Soal 1

var firstLoop = 2;
console.log('LOOPING PERTAMA');
while (firstLoop <= 20) {
  console.log(firstLoop + ' - I love coding');
  firstLoop += 2;
}

var secondLoop = 20;
console.log('LOOPING KEDUA');
while (secondLoop > 0) {
  console.log(secondLoop + ' - I will become a frontend developer');
  secondLoop -= 2;
}

console.log('==========================================================================================');

// Soal 2

for (var i = 1; i <= 20; i++) {
  if (i % 3 == 0 && i % 2 !== 0) {
    console.log(i + ' - I Love Coding');
  } else if (i % 2 == 0) {
    console.log(i + ' - Berkualitas');
  } else if (i % 2 == 1) {
    console.log(i + ' - Santai');
  }
};

console.log('==========================================================================================');

// Soal 3

var hastag = '';

for (var i = 0; i < 7; i++) {
  for (let j = 0; j < 7; j++) {
    if (i >= j) {
      hastag += '#';
    };
  };
  hastag += '\n';
};

console.log(hastag);

console.log('==========================================================================================');

// Soal 4

var kalimat = "saya sangat senang belajar javascript";
var array = kalimat.split(' ');

console.log(array);

console.log('==========================================================================================');

// Soal 5

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];

var urutanBuah = daftarBuah.sort(function (a, b) {
  return a[0] - b[0];
});

console.log(urutanBuah.join(' '));