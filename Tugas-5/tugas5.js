// Soal 1

function halo() {
  return 'Halo Sanbers!';
};

console.log(halo()); // "Halo Sanbers!" 

console.log('==========================================================================================');

// Soal 2

function kalikan(param1, param2) {
  return param1 * param2;
};

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 48

console.log('==========================================================================================');

// Soal 3

function introduce(nama, umur, alamat, hobi) {
  return "Nama saya " + nama + " umur saya " + umur + " tahun, alamat saya di " + alamat + ", dan saya punya hobby yaitu " + hobi + "!";
};

var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!" 

console.log('==========================================================================================');

// Soal 4

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992];
var pesertaObj = {
  nama: arrayDaftarPeserta[0],
  jenisKelamin: arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  tahunLahir: arrayDaftarPeserta[3]
}

console.log(pesertaObj);

console.log('==========================================================================================');

// Soal 5

var arrBuah = [
  {
    nama: 'strawberry',
    warna: 'merah',
    adaBijinya: 'tidak',
    harga: 9000
  },
  {
    nama: 'jeruk',
    warna: 'oranye',
    adaBijinya: 'ada',
    harga: 8000
  },
  {
    nama: 'Semangka',
    warna: 'Hijau & Merah',
    adaBijinya: 'ada',
    harga: 10000
  },
  {
    nama: 'Pisang',
    warna: 'Kuning',
    adaBijinya: 'tidak',
    harga: 5000
  }
];
var firstIndex = '1. nama: ' + arrBuah[0].nama + '\nwarna: ' + arrBuah[0].warna + '\nadaBijinya: ' + arrBuah[0].adaBijinya + '\nharga: ' + arrBuah[0].harga;
console.log(firstIndex);

console.log('==========================================================================================');

// Soal 6

var dataFilm = [];

function addFilm(nama, durasi, genre, tahun) {
  var daftarFilm = {
    nama: nama,
    durasi: durasi,
    genre: genre,
    tahun: tahun
  };
  dataFilm.push(daftarFilm);
};

addFilm('Avengers: Endgame', '181 Menit', 'Action', '2019');
addFilm('The Fate of the Furious', '2 jam 16 menit', 'Action', '2017');
console.log(dataFilm);