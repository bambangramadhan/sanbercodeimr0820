// Soal 1

var readBooks = require('./callback.js')

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
  { name: 'komik', timeSpent: 1000 }
];

// Tulis code untuk memanggil function readBooks di sini

let i = 0;
function callback(time) {
  i++;
  if (i < books.length) {
    if (time >= books[i].timeSpent) {
      readBooks(time, books[i], callback)
    };
  };
};

readBooks(10000, books[0], callback);