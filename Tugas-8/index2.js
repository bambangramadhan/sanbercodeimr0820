// Soal 2

var readBooksPromise = require('./promise.js')

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise 
let i = 0;

function readBooks(time) {
  if (i < books.length) {
    readBooksPromise(time, books[i])
      .then((fulfilled) => {
        i++;
        readBooks(fulfilled);
      })
      .catch(error => {
        console.log(error);
      });
  };
};

readBooks(10000);