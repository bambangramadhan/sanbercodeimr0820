// Soal 1

const luasLingkaran = (jari_jari) => {
  return Math.round(Math.PI * (jari_jari * jari_jari))
};

const kelilingLingkaran = (jari_jari) => {
  return Math.round(2 * Math.PI * jari_jari)
};

console.log(luasLingkaran(7));
console.log(kelilingLingkaran(7));

console.log('==========================================================================================');

// Soal 2

let kalimat = "";

const name = (kata1, kata2, kata3, kata4, kata5) => {
  kalimat = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`;
};

name('saya', 'adalah', 'seorang', 'frontend', 'developer');
console.log(kalimat);

console.log('==========================================================================================');

// Soal 3

const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
      return;
    }
  };
};

//Driver Code 
newFunction("William", "Imoh").fullName();

console.log('==========================================================================================');

// Soal 4

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
};

const { firstName, lastName, destination, occupation, spell } = newObject;

console.log(firstName, lastName, destination, occupation);

console.log('==========================================================================================');

// Soal 5

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined);