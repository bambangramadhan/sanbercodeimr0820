// soal 1

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

console.log(kataPertama + ' ' + kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1) + ' ' + kataKetiga + ' ' + kataKeempat.toUpperCase());

console.log('==========================================================================================');

// soal 2

var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

console.log(parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat));

console.log('==========================================================================================');

// soal 3

var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 15); // do your own! 
var kataKetiga = kalimat.substring(15, 19); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25); // do your own! 

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);

console.log('==========================================================================================');

// soal 4

var nilai = 75;

if (nilai >= 80 && nilai <= 100) {
  console.log('A');
} else if (nilai >= 70 && nilai < 80) {
  console.log('B');
} else if (nilai >= 60 && nilai < 70) {
  console.log('C');
} else if (nilai >= 50 && nilai < 60) {
  console.log('D');
} else if (nilai < 50 && nilai >= 0) {
  console.log('E');
} else {
  console.log('Format nilai salah');
};

console.log('==========================================================================================');

// soal 5

var tanggal = 4;
var bulan = 9;
var tahun = 2000;
var namaBulan;

switch (bulan) {
  case 1:
    namaBulan = 'Januari'
    break;

  case 2:
    namaBulan = 'Februari'
    break;

  case 3:
    namaBulan = 'Maret'
    break;

  case 4:
    namaBulan = 'April'
    break;

  case 5:
    namaBulan = 'Mei'
    break;

  case 6:
    namaBulan = 'Juni'
    break;

  case 7:
    namaBulan = 'Juli'
    break;

  case 8:
    namaBulan = 'Agustus'
    break;

  case 9:
    namaBulan = 'September'
    break;

  case 10:
    namaBulan = 'Oktober'
    break;

  case 11:
    namaBulan = 'November'
    break;

  case 12:
    namaBulan = 'Desember'
    break;

  default:
    break;
};

if ((tanggal >= 1 && tanggal <= 31) && (bulan >= 1 && bulan <= 12) && (tahun >= 1900 && tahun <= 2200)) {
  console.log(tanggal + ' ' + namaBulan + ' ' + tahun);
} else {
  console.log('Anda salah memasukkan format!');
};